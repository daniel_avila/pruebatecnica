require("dotenv").config();

const app = require("./src/app");

const { API_PORT } = require("./config");

const start = () => {
  app.listen(API_PORT, () => {
    console.info(`Server running on port: ${API_PORT}`);
  });
};

start();
