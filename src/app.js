const express = require("express");
const morgan = require("morgan");
const appRouter = require("./routes");
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(morgan("tiny"));
app.use(appRouter);

module.exports = app;
