const { Router } = require("express");

const empleadoRoutes = require("./empleado/empleado.routes");

const appRouter = new Router();

appRouter.use("/empleado", empleadoRoutes);

module.exports = appRouter;
