const { Router } = require("express");
const empleadoController = require("../../controllers/empleado/empleado.controller");

const empleadoRouter = new Router();

empleadoRouter.get("/", empleadoController.getMany);
empleadoRouter.get("/:id", empleadoController.getOne);
empleadoRouter.post("/", empleadoController.createOne);
empleadoRouter.patch("/:id", empleadoController.updateOne);
empleadoRouter.delete("/:id", empleadoController.deleteOne);

module.exports = empleadoRouter;
