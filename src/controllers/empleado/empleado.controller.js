const { PrismaClient } = require("@prisma/client");
const Joi = require("joi");

const prisma = new PrismaClient();

const getMany = async (_req, res) => {
  try {
    const empleados = await prisma.empleado.findMany({
      include: { departamento: true },
    });
    return res.status(200).json({ empleados });
  } catch (error) {
    return res
      .status(500)
      .json({ message: "An error has ocurred", error: error.message });
  }
};

const getOne = async (req, res) => {
  try {
    const { id } = req.params;
    const empleado = await prisma.empleado.findUnique({
      where: { codigo: parseInt(id) },
      include: { departamento: true },
    });

    if (!empleado) {
      return res.status(404).json({ message: "Empleado not found" });
    }
    return res.status(200).json({ empleado });
  } catch (error) {
    return res
      .status(500)
      .json({ message: "An error has ocurred", error: error.message });
  }
};

const createOne = async (req, res) => {
  const schema = Joi.object({
    nif: Joi.string().alphanum().length(9).required(),
    nombre: Joi.string().alphanum().max(100).required(),
    apellido1: Joi.string().alphanum().max(100).required(),
    apellido2: Joi.string().alphanum().max(100),
    codigo_departamento: Joi.number().integer().required(),
  });

  try {
    const { body } = req;
    const { error, value } = schema.validate(body, { abortEarly: false });
    if (error) {
      return res
        .status(400)
        .json({ message: "Invalid data recevied", error: error.details });
    }

    const existentEmpleado = await prisma.empleado.findUnique({
      where: { codigo: parseInt(id) },
    });

    if (existentEmpleado) {
      return res
        .status(400)
        .json({ message: `A record with id ${id} already exists` });
    }

    const empleado = await prisma.empleado.create({
      data: value,
    });

    return res.status(200).json({ empleado });
  } catch (error) {
    return res
      .status(500)
      .json({ message: "An error has ocurred", error: error.message });
  }
};

const updateOne = async (req, res) => {
  const schema = Joi.object({
    nif: Joi.string().alphanum().length(9),
    nombre: Joi.string().alphanum().max(100),
    apellido1: Joi.string().alphanum().max(100),
    apellido2: Joi.string().alphanum().max(100),
    codigo_departamento: Joi.number().integer(),
  });

  try {
    const { body, params } = req;
    const { error, value } = schema.validate(body, { abortEarly: false });
    if (error) {
      return res
        .status(400)
        .json({ message: "Invalid data recevied", error: error.details });
    }

    const existentEmpleado = await prisma.empleado.findUnique({
      where: { codigo: parseInt(id) },
    });

    if (!existentEmpleado) {
      return res.status(404).json({ message: "Empleado not found" });
    }

    const empleado = await prisma.empleado.update({
      data: value,
      where: { codigo: parseInt(params.id) },
      include: { departamento: true },
    });

    return res.status(200).json({ empleado });
  } catch (error) {
    return res
      .status(500)
      .json({ message: "An error has ocurred", error: error.message });
  }
};

const deleteOne = async (req, res) => {
  try {
    const { id } = req.params;

    const empleado = await prisma.empleado.findUnique({
      where: { codigo: parseInt(id) },
    });

    if (!empleado) {
      return res.status(404).json({ message: "Empleado not found" });
    }

    await prisma.empleado.delete({
      where: { codigo: parseInt(id) },
    });

    return res.status(204);
  } catch (error) {
    return res
      .status(500)
      .json({ message: "An error has ocurred", error: error.message });
  }
};

module.exports = { getMany, getOne, createOne, updateOne, deleteOne };
