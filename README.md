# Prueba Técnica

Microservicio empleado

## Requisitos

-POSTGRESQL

-Node >= 14

## Instalación

1. Instalar dependencias con npm

```bash
npm install
```

2. Crear archivo de .env y reemplazar la DATABASE_URL con el string de conexion.

```bash
API_PORT=1234
DATABASE_URL="postgresql://user:password@localhost:5432/dataBase?schema=public"
```

3. Correr migraciones

```bash
npx prisma migrate deploy
```

## Correr el servicio

```bash
npm start
```

## Modo de uso

- Correr Prisma Studio

  ```bash
  npx prisma studio
  ```

- Crear `departamentos` desde Prisma Studio:

  Ingresar a http://localhost:5555

Usar cliente HTTP para usar los endpoints de empleado

### Endpoints

- Previamente se deben crear departamentos en la BD

- Crear un empleado:

  - POST /empleado

    Body:

    ```json
    {
      "nif": "123456789",
      "nombre": "Pedrito",
      "apellido1": "Gomez",
      "codigo_departamento": 1
    }
    ```

- Listar empleados:

  - GET /empleado

- Obtener un empleado:

  - GET /empleado/:id

- Editar un empleado:

  - PATCH /empleado/:id

    Body:

    ```json
    {
      "nif": "123456789",
      "nombre": "Pedrito editado",
      "apellido1": "Gomez",
      "codigo_departamento": 2
    }
    ```

- Eliminar un empleado:

  - DELETE /empleado/:id
