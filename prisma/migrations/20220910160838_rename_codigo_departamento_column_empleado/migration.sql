/*
  Warnings:

  - You are about to drop the column `departamentoCodigo` on the `empleado` table. All the data in the column will be lost.
  - Added the required column `codigo_departamento` to the `empleado` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "empleado" DROP CONSTRAINT "empleado_departamentoCodigo_fkey";

-- AlterTable
ALTER TABLE "empleado" DROP COLUMN "departamentoCodigo",
ADD COLUMN     "codigo_departamento" INTEGER NOT NULL;

-- AddForeignKey
ALTER TABLE "empleado" ADD CONSTRAINT "empleado_codigo_departamento_fkey" FOREIGN KEY ("codigo_departamento") REFERENCES "departamento"("codigo") ON DELETE RESTRICT ON UPDATE CASCADE;
