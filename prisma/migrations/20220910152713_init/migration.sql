-- CreateTable
CREATE TABLE "empleado" (
    "codigo" SERIAL NOT NULL,
    "nif" TEXT NOT NULL,
    "nombre" TEXT NOT NULL,
    "apellido1" TEXT NOT NULL,
    "apellido2" TEXT,
    "departamentoCodigo" INTEGER NOT NULL,

    CONSTRAINT "empleado_pkey" PRIMARY KEY ("codigo")
);

-- CreateTable
CREATE TABLE "departamento" (
    "codigo" SERIAL NOT NULL,
    "nombre" TEXT NOT NULL,
    "presupuesto" DECIMAL(65,30) NOT NULL,

    CONSTRAINT "departamento_pkey" PRIMARY KEY ("codigo")
);

-- CreateIndex
CREATE UNIQUE INDEX "empleado_codigo_key" ON "empleado"("codigo");

-- CreateIndex
CREATE UNIQUE INDEX "departamento_codigo_key" ON "departamento"("codigo");

-- AddForeignKey
ALTER TABLE "empleado" ADD CONSTRAINT "empleado_departamentoCodigo_fkey" FOREIGN KEY ("departamentoCodigo") REFERENCES "departamento"("codigo") ON DELETE RESTRICT ON UPDATE CASCADE;
